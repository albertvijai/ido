

<div class="timelineLoader">
    <img src="libs/timeline/images/timeline/loadingAnimation.gif" />
</div>
<!-- BEGIN TIMELINE -->
<div id="myTimeline" class="timelineFlat timelineFlatBlog tl2">
    <div class="item" data-id="04/05/2011" data-description="Test 1">
        <a class="image_rollover_bottom con_borderImage" data-description="ZOOM IN" href="images/intro.jpg" rel="lightbox[timeline]">
            <img src="images/intro.jpg" alt=""/>
        </a>
        <div class="post_date">4<span>MAY</span></div>
        <h2>MAY, 4</h2>
        <span>WOW test 1</span>
    </div>
    <div class="item" data-id="05/06/2012" data-description="Lorem ipsum">
        <a class="image_rollover_bottom con_borderImage" data-description="ZOOM IN" href="images/flat/blog/1.jpg" rel="lightbox[timeline]">
            <img src="images/intro.jpg" alt=""/>
        </a>
        <div class="post_date">4<span>MAY</span></div>
        <h2>MAY, 4</h2>
        <span>test</span>
    </div>
    <div class="item" data-id="01/01/2013" data-description="Lorem ipsum">
        <a class="image_rollover_bottom con_borderImage" data-description="ZOOM IN" href="images/flat/blog/1.jpg" rel="lightbox[timeline]">
            <img src="images/intro.jpg" alt=""/>
        </a>
        <div class="post_date">4<span>MAY</span></div>
        <h2>MAY, 4</h2>
        <span>test</span>
    </div>
</div> <!-- /END TIMELINE -->

<script>
    $(window).load(function() {
        $('#myTimeline').timeline();
    });	
</script>




