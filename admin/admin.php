<?php
include("../db/config.php");

$maxInvIdSql = "SELECT MAX(ID) id FROM invitation";
$maxInvIdSqlResult = mysql_query($maxInvIdSql);
$maxInvIdRow = mysql_fetch_array($maxInvIdSqlResult);
$maxInvId = $maxInvIdRow['id'];
$maxInvId++;

$countGuestSql = "select count(distinct(invitation_id)) invitation_count,count(*) guest_count from guest";
$countGuestSqlResult = mysql_query($countGuestSql);
$countGuest = mysql_fetch_array($countGuestSqlResult);
$totalGuest = $countGuest['guest_count'];
$totalInvitation = $countGuest['invitation_count'];
       
function generateRandomInviteKey($length) {
    $characters = 'CHANDVJ103';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}

if(($_SERVER["REQUEST_METHOD"] == "GET") && isset($_GET['action'])){
    $error = false;
    
    $action = $_GET['action'];
    $selectedRow = $_GET['selectedRow'];
    
    if('ADD'==$action){

        $mailing_contact = addslashes($_GET['invitation_name']);
        $address = addslashes($_GET['address']);
        $phone = addslashes($_GET['phone']);
        $invite_key = generateRandomInviteKey(3).str_pad($maxInvId, 3, "0", STR_PAD_LEFT);
        
        if(null==$mailing_contact || null==$address || null==$phone){
            $error = true;
        }
        else{
            $insertInvitation = "INSERT INTO invitation (invite_key, invitation_name, address, phone) "
                               ."values ('".$invite_key."', '".$mailing_contact."', '".$address."', '".$phone."');";

            if(mysql_query($insertInvitation)){
                header("location: admin.php");
            }
        }
    }
    else if('DELETE'==$action){
        $deleteInvitation = "DELETE FROM invitation WHERE id = ".$selectedRow;
        if(mysql_query($deleteInvitation)){
            header("location: admin.php");
        }
    }
    else if('GUEST'==$action){
            header("location: guestCRUD.php?invitationId=".$selectedRow);
    }
}
?>

<html>
    
<head>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
</head>

<body>
    <?php include './headerAdmin.php';?><br/><br/> 
    <div class="container">
        <div class="panel panel-default">
            <h4>&nbsp;&nbsp;New Invitation</h4>
        <table class="table table-hover">
            <tr>
                <td>
                    <form  id="addInviteForm"class="form-inline"  role="form">
                            <input type="hidden" id="action"  name="action" value=""/>
                            <input type="hidden" id="selectedRow"  name="selectedRow" value=""/>
                            <div class="form-group">
                                <input type="text" class="form-control" id="mailing_contact" size="25" name="invitation_name" maxlength="100" placeholder="Invitation Name"/>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="address" size=30" name="address" maxlength="200" placeholder="Address">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="phone" name="phone" maxlength="10" placeholder="Phone">
                            </div>
                            
                            <button id="addInvite" type="button" class="btn btn-danger addInvite">Add</button>
                    </form>
                </td>
            </tr>
        </table>
        </div>
        <?php 
            $invitationSql = "SELECT * FROM invitation order by invitation_name";
            $invitationSqlResult = mysql_query($invitationSql);
        ?>
        <h4>Edit Invitations</h4>
        <div class="alert alert-info"> Invitations: <?php echo $totalInvitation?>, Guests: <?php echo $totalGuest?></div>
        <table class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Invitation Key</th>
                <th>Invitation Name</th>
                <th>Address</th>
                <th>Phone</th>
                <th>Guest(s)</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody id='invitation'>
            <?php
                while ($invitation = mysql_fetch_array($invitationSqlResult)) {
                    
                    $guestCountSql = "SELECT count(*) count FROM guest where invitation_id = ".$invitation['id'];
                    $guestCountSqlResult = mysql_query($guestCountSql);
                    $guestCount = mysql_fetch_array($guestCountSqlResult);
                    echo '<tr class="btnRow" data-id="'.$invitation['id'].'">';
                        echo '<td>'. $invitation['invite_key'].'</td>';
                        echo '<td><a href="#" data-pk="'.$invitation['id'].'" id="invitation_name" data-type="text" title="Edit Invitation name" >'.$invitation['invitation_name'].'</a></td>';
                        echo '<td><a href="#" data-pk="'.$invitation['id'].'" id="address" data-type="text" title="Enter address" >'.$invitation['address'].'</a></td>';
                        echo '<td><a href="#" data-pk="'.$invitation['id'].'" id="phone" data-type="text" title="Enter Phone#" >'.$invitation['phone'].'</a></td>'; 
                        echo '<td>'.$guestCount['count'].'</td>';
                        echo '<td>'
                                . '<button type="button" class="btn btn-info btnbtnRowEdit">Edit</button>&nbsp;&nbsp;'
                                . '<button type="button" class="btn btn-danger btnbtnRowDelete">Delete</button>'
                            . '</td>';
                     echo '</tr>';
                }
            ?>
            </tbody>
        </table>
    </div>
    
    <script>   
       $(function(){
           
            $('button.addInvite').on('click', function (e) {
               e.preventDefault();
               $('#action').val("ADD"); 
               $('#addInviteForm').submit();
            });
           
            $('button.btnbtnRowDelete').on('click', function (e) {
               e.preventDefault();
               $('#action').val("DELETE"); 
               $('#selectedRow').val($(this).closest('tr').data('id')); 
               $('#addInviteForm').submit();
           });
           
           $('button.btnbtnRowEdit').on('click', function (e) {
               e.preventDefault();
               $('#action').val("GUEST"); 
               $('#selectedRow').val($(this).closest('tr').data('id')); 
               $('#addInviteForm').submit();
           });
           
            $('#invitation a').editable({
               mode: 'inline', 
               url: 'invitationPost.php'
            });
        });
    </script>
    
  </body>
</html>
