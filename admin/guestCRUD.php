<?php
include("../db/config.php");


$invitationId = $_GET['invitationId'];
$invitationSql = "SELECT invitation_name FROM invitation where id =".$invitationId;
$invitationSqlResult = mysql_query($invitationSql);
$inviteRow = mysql_fetch_array($invitationSqlResult);

if(($_SERVER["REQUEST_METHOD"] == "GET") && isset($_GET['action'])){
    $error = false;
    $action = $_GET['action'];
    if('ADD'==$action){

        $first_name = addslashes($_GET['first_name']);
        $last_name = addslashes($_GET['last_name']);
        
        if(null==$first_name || null==$first_name){
            $error = true;
        }
        else{
            $insertGuest = "INSERT INTO guest (invitation_id, first_name, last_name) "
                               ."values (".$invitationId.", '".$first_name."', '".$last_name."');";

            if(mysql_query($insertGuest)){
                header("location: guestCRUD.php?invitationId=".$invitationId);
            }
        }
    }
    else if('DELETE'==$action){
        $guestId = $_GET['guestId'];
        $deleteguest = "DELETE FROM guest WHERE id = ".$guestId;
        if(mysql_query($deleteguest)){
            header("location: guestCRUD.php?invitationId=".$invitationId);
        }
    }
}


?>

<html>
    
<head>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
</head>

<body>
    <?php include './headerAdmin.php';?><br/>
    <div class="container">
        <div class="panel panel-default">
        <h4>&nbsp;&nbsp;Create New Guest for Invitation, <a href="/ido/admin/admin.php"><?php echo $inviteRow['invitation_name'];?></a></h4>
        <table class="table table-hover">
            <tr>
                <td>
                    <form  id="addGuestForm"class="form-inline"  role="form">
                            <input type="hidden" id="action"  name="action" value=""/>
                            <input type="hidden" id="invitationId"  name="invitationId" value="<?php echo $invitationId;?>"/>
                            <input type="hidden" id="guestId"  name="guestId" value=""/>
                            <div class="form-group">
                                <input type="text" class="form-control" id="first_name" name="first_name" maxlength="100" placeholder="First Name"/>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="last_name" name="last_name" maxlength="100" placeholder="Last Name"/>
                            </div>
                            <button id="addInvite" type="button" class="btn btn-danger addGuest">Add</button>
                    </form>
                </td>
            </tr>
        </table>
        </div>
        
        <h4>Edit Guest(s)</h4>
                <table class="table table-hover table-bordered">
            <thead>
              <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Special Needs</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody id='guest'>
            <?php
                $guestSql="SELECT * FROM guest WHERE invitation_id = $invitationId";
                $guestResult=mysql_query($guestSql);
                while ($guest = mysql_fetch_array($guestResult)) {
                         echo '<tr class="btnRow" data-id="'.$guest['id'].'">';
                         echo '<td><a href="#" data-pk="'.$guest['id'].'" id="first_name" data-type="text"     title="Enter First Name" >'.$guest['first_name'].'</a></td>'; 
                         echo '<td><a href="#" data-pk="'.$guest['id'].'" id="last_name" data-type="text"     title="Enter Last Name" >'.$guest['last_name'].'</a></td>';
                         echo '<td><a href="#" data-pk="'.$guest['id'].'" id="email" data-type="text"     title="Enter email address" >'.$guest['email'].'</a></td>'; 
                         echo '<td><a href="#" data-pk="'.$guest['id'].'" id="notes" data-type="textarea" title="Enter notes" data-mode="popup">'.$guest['notes'].'</a></td>'; 
                         echo '<td><button type="button" class="btn btn-danger gustRowDelete">Delete Guest</button></td>';
                         echo '</tr>';
                }
            ?>
            </tbody>
        </table>
    </div>
    
    <script>   
       $(function(){
           
            $('button.addGuest').on('click', function (e) {
               e.preventDefault();
               $('#action').val("ADD"); 
               $('#addGuestForm').submit();
            });
           
            $('button.gustRowDelete').on('click', function (e) {
               e.preventDefault();
               $('#action').val("DELETE"); 
               $('#guestId').val($(this).closest('tr').data('id')); 
               $('#addGuestForm').submit();
           });
           
            $('#guest a').editable({
               mode: 'inline', 
               url: 'guestPost.php'
            });
        });
    </script>
    
  </body>
</html>
