<?php
$inviteID = $_SESSION['inviteID'];
$guestSql="SELECT * FROM guest WHERE invitation_id = $inviteID";
$guestResult=mysql_query($guestSql);


$mealOptions = array(
  array('value' => 'Chicken', 'text' => 'Chicken'),
  array('value' => 'Vegeterian', 'text' => 'Vegeterian')
);


$mealOptionsJson = json_encode($mealOptions);

?>

<div class="modal fade bs-example-modal-lg" id="rsvpModal" tabindex="-1" role="dialog" aria-labelledby="rsvpModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-lg">
      <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="myModalLabel">RSVP</h4>
            </div>
            <div class="modal-body offset3 span6">
                <table class="table">
                    <thead>
                      <tr>
                        <th>Guest Name</th>
                        <th>RSVP</th>
                        <th>Meal</th>
                        <th>Email</th>
                        <th>Table</th>
                        <th>Special Needs</th>
                      </tr>
                    </thead>
                    <tbody id='guest'>
                    <?php
                        while ($guest = mysql_fetch_array($guestResult)) {
                            if($guest['rsvp']==null){//Editable
                                echo '<tr>';
                                    echo '<td>'. $guest['first_name'] . ' '. $guest['last_name'] . '</td>';
                                    echo '<td><a href="#" data-pk="'.$guest['id'].'" id="rsvp"  data-type="select"   data-source="admin/rsvpJson.php" >'.$guest['rsvp'].'</a></td>'; 
                                    echo '<td><a href="#" data-pk="'.$guest['id'].'" id="meal"  data-type="select"   data-source="admin/mealJson.php" >'.$guest['meal'].'</a></td>'; 
                                    echo '<td><a href="#" data-pk="'.$guest['id'].'" id="email" data-type="text"     title="Enter email address" >'.$guest['email'].'</a></td>'; 
                                    echo '<td>'. $guest['table'] .'</td>';
                                    echo '<td><a href="#" data-pk="'.$guest['id'].'" id="notes" data-type="text" title="Enter notes" data-mode="popup">'.$guest['notes'].'</a></td>';
                                 echo '</tr>';
                            }
                            else{
                                echo '<tr>';
                                    echo '<td>'.$guest['first_name'] . ' '. $guest['last_name'] . '</td>';
                                    echo '<td>'.$guest['rsvp'].'</td>'; 
                                    echo '<td>'.$guest['meal'].'</td>'; 
                                    echo '<td><a href="#" data-pk="'.$guest['id'].'" id="email" data-type="text"     title="Enter email address" >'.$guest['email'].'</a></td>'; 
                                    echo '<td>'. $guest['table'] .'</td>';
                                    echo '<td><a href="#" data-pk="'.$guest['id'].'" id="notes" data-type="text" title="Enter notes" data-mode="popup">'.$guest['notes'].'</a></td>'; 
                                 echo '</tr>';
                            }

                        }
                    ?>
                    </tbody>
                </table>
            </div>
      </div>
    </div>
</div>


<script>   
    
   $(function(){
        $('#guest a').editable({
           mode: 'inline', 
           url: 'admin/guestPost.php'
        });
    });
</script>
