<?php
include("../db/config.php");

$sqlResult = null;
if(($_SERVER["REQUEST_METHOD"] == "GET") && isset($_GET['sql'])){
    $isTblUpdate = false;
    $sql = $_GET['sql'];
    
    if (strpos($sql,'delete') !== false ||strpos($sql,'truncate') !== false) {
        $sql = "SELECT 'CANNOT DELETE/TRUNCATE.'";
    }

    $sqlResult = mysql_query($sql);
}
?>

<html>
    
<head>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
</head>

<body>
    <?php include './headerAdmin.php';?><br/><br/> 
    <div class="container">
        <form  id="sqlForm"  role="form">
            <div>
                <textarea id="sql" name="sql" class="form-control" rows="6" placeholder="SQL query"></textarea>
            </div><br/>
            <button id="addInvite" type="submit" class="btn btn-danger addGuest">SQL</button>
        </form>
        
        <?php
        if(null!=$sqlResult){
            echo '<h4>Result</h4>';
            echo '<table class="table table-hover">';
            $columns =  mysql_num_fields($sqlResult);
            
            while ($sqlRow = mysql_fetch_array($sqlResult)) {
                echo '<tr>';
                    $i = 0;
                    while ($i < $columns) {
                       echo '<td>'.$sqlRow[$i].'</td>';
                       $i++;
                    }
                echo '</tr>';
            }
            echo '</table>';
        }
        ?>
        
    </div>
    
  </body>
</html>
