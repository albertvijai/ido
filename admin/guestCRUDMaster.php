<?php
include("../db/config.php");
?>

<html>
    
<head>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
</head>

<body>
    <?php include './headerAdmin.php';?><br/><br/> 
    <div class="container">
        <h4>Guest(s)</h4>
            <table class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>RSVP</th>
                <th>Meal</th>
                <th>Table#</th>
                <th>Email</th>
                <th>Special Needs</th>
              </tr>
            </thead>
            <tbody id='guest'>
            <?php
                $guestSql="SELECT * FROM guest order by invitation_id";
                $guestResult=mysql_query($guestSql);
                while ($guest = mysql_fetch_array($guestResult)) {
                         echo '<tr class="btnRow" data-id="'.$guest['id'].'">';
                         echo '<td><a href="#" data-pk="'.$guest['id'].'" id="first_name" data-type="text"     title="Enter First Name" >'.$guest['first_name'].'</a></td>'; 
                         echo '<td><a href="#" data-pk="'.$guest['id'].'" id="last_name" data-type="text"     title="Enter Last Name" >'.$guest['last_name'].'</a></td>';
                         echo '<td><a href="#" data-pk="'.$guest['id'].'" id="rsvp"  data-type="select"   data-source="rsvpJson.php" >'.$guest['rsvp'].'</a></td>'; 
                         echo '<td><a href="#" data-pk="'.$guest['id'].'" id="meal"  data-type="select"   data-source="mealJson.php" >'.$guest['meal'].'</a></td>'; 
                         echo '<td><a href="#" data-pk="'.$guest['id'].'" id="table" data-type="select"   data-source="tableJson.php" >'.$guest['table'].'</a></td>';
                         echo '<td><a href="#" data-pk="'.$guest['id'].'" id="email" data-type="text"     title="Enter email address" >'.$guest['email'].'</a></td>'; 
                         echo '<td><a href="#" data-pk="'.$guest['id'].'" id="notes" data-type="textarea" title="Enter notes" data-mode="popup">'.$guest['notes'].'</a></td>'; 
                         echo '</tr>';
                }
            ?>
            </tbody>
        </table>
    </div>
    
    <script>   
       $(function(){

            $('#guest a').editable({
                   mode: 'inline', 
                   url: 'guestPost.php'
                });
            });
        
    </script>
    
  </body>
</html>
