$(function(){
	
	var note = $('#note'),
		ts = new Date(2014, 9, 3, 6, 30),
		isB4W = true;
	
	if((new Date()) > ts){
		isB4W = false;
	}
		
	$('#countdown').countdown({
           	timestamp	: ts,
		callback	: function(days, hours, minutes, seconds){
			var message = "";
                        if(isB4W){
                                message = "";
                            	message += days + " day" + ( days==1 ? '':'s' ) + ", ";
                                message += hours + " hour" + ( hours==1 ? '':'s' ) + ", ";
                                //message += minutes + " minute" + ( minutes==1 ? '':'s' ) + " and ";
                                //message += seconds + " second" + ( seconds==1 ? '':'s' ) + " <br />";

                                note.html(message);
                        }
		}
	});
	
});
