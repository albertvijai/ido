<?php
include("./db/config.php");
session_start();
$error = false;
if(($_SERVER["REQUEST_METHOD"] == "GET") && isset($_GET['inviteKey'])){
    $inviteKey=addslashes($_GET['inviteKey']); 
    $sql="SELECT id FROM invitation WHERE invite_key='$inviteKey'";
    $result=mysql_query($sql);
    $row=mysql_fetch_array($result);
    //$active=$row['active'];
    $inviteID=$row['id'];
    $count=mysql_num_rows($result);

    if($count==1){//Valid user
        $_SESSION['inviteID']=$inviteID;
        header("location: welcome.php");
    }
    else{
        $error = true;
    }
}
?>
<html>
    <head>
        
        <title>V&C!</title>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        
        <style>
            
            body {
		background: url('media/images/glass.png'), url('images/login.jpg');
		background-size: 256px 256px, cover;
		background-attachment: fixed, fixed;
		background-position: top left, bottom center;
		background-repeat: repeat, no-repeat;
            }
            
            .absolute-center{
                width: 40%;
                height: 40%;
                overflow: auto;
                margin: auto;
                position: absolute;
                table-layout: initial;
                top: 0; left: 0; bottom: 0; right: 0;
            }

        </style>
        
    </head>
    <body>
        
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" id="myModalLabel">Login help!</h4>
                </div>
                <div class="modal-body offset3 span6">
                    <h6>
                        Invitation ID is a 6 digit ID that can be found at the back of your "Save the Date" card we mailed you.
                        If you still have trouble logging in, please email us at <b>chandnivijai@gmail.com. We will reach out to you as soon as we can.<br/>
                        <u>Apologize for the inconvenience.</u><br/><br/>
                            <img class="saveDate" src="images/saveTheDate.jpg" width="95%"/><br/><br/>
                    </h6>
                </div>
              </div>
            </div>
        </div>
        
        <div class="absolute-center">
            <form class="form-inline" role="form">
                    
                    <?php include './countdown.html'; ?>
                    <div class="input-group col-lg-6" style="margin:0 auto;">
                        <input type="password" maxlength="6" class="form-control" id="inviteKey" name="inviteKey" placeholder="Enter Wedding Invitation ID">
                        <span class="input-group-btn">
                            <button class="btn btn-danger" type="submit">Go!</button>
                        </span>
                    </div>

            </form>
            <?php
                if($error){
                    echo '<div class="input-group col-lg-6" style="margin:0 auto;"><br/>';
                    echo ' <div class="alert alert-danger"> Invitation ID is incorrect.';
                    echo '   <a href="#" data-toggle="modal" data-target="#myModal">Login Help!</a>';
                    echo ' </div>';
                    echo '</div>';                
                }
            ?>
        </div>
    </body>
</html>
