<?php
    include('./db/config.php');
    include("./db/lock.php");
?>
<html>
    <head>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

        <link rel="stylesheet" href="http://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
        <link rel="stylesheet" href="libs/bootstrap-image-gallery/css/bootstrap-image-gallery.min.css">
        <link rel="stylesheet" href='http://fonts.googleapis.com/css?family=Montserrat' type='text/css'>
        <link rel="stylesheet" href="libs/timeline/css/flat.css" type="text/css" />
        <link rel="stylesheet" href="libs/timeline/css/style.css" type="text/css" />
        <link rel="stylesheet" href="libs/timeline/css/lightbox.css"  type="text/css" />
        <link rel="stylesheet" href="libs/timeline/css/jquery.mCustomScrollbar.css"  type="text/css" />
    </head>
    <body>
        <header id="header">
            <span><h1>V&C</h1><a href="#" data-toggle="modal" data-target="#rsvpModal">RSVP</a></span>
        </header><br/><br/>
        
        <section>
            <?php
                require 'admin/rsvpModal.php';
            ?>
        </section><br/><br/>
        <section>
            <?php
                include 'timeLine.php';
            ?>
        </section><br/><br/>
        <section>
            <?php
                include 'pictureGallery.php';
            ?>
        </section><br/><br/>

        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
        <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

        <script src="http://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
        <script src="libs/bootstrap-image-gallery/js/bootstrap-image-gallery.min.js"></script>
        <script src="libs/bootstrap-image-gallery/js/getFlickrImg.js"></script>
        <script src="libs/timeline/js/jquery.mCustomScrollbar.js"></script>
        <script src="libs/timeline/js/jquery.easing.1.3.js"></script>
        <script src="libs/timeline/js/jquery.timeline.min.js"></script>
        <script src="libs/timeline/js/image.js"></script>
        <script src="libs/timeline/js/lightbox.js"></script>
  
    </body>
</html>